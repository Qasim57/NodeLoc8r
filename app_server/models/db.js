var mongoose = require('mongoose');
require('./locations');
var dbURI;
if (process.env.NODE_ENV === 'production') {
	dbURI = 'mongodb://Loc8r_user:FunnyBunny@ds017582.mlab.com:17582/heroku_w9s4x3pc';
} else {
	dbURI = 'mongodb://localhost/Loc8r';
}
mongoose.connect(dbURI);

mongoose.connection.on('connected', function () {
	console.log('Connected to ' + dbURI);
});

mongoose.connection.on('error', function (error) {
	console.log('Mongoose connection error: ' + error);
});

mongoose.connection.on('disconnected', function () {
	console.log('Mongoose disconnected');
});


//Closing the Mongoose connection when the application stops is a best-practice, handling that here
var gracefulShutdown = function (msg, callback) {
	mongoose.connection.close(function () {
		console.log('Mongoose disconnected through ' + msg);
		callback();
	});
};

//For nodemon restarts
process.once('SIGUSR2', function () {
	gracefulShutdown('nodemon restart', function () {
		process.kill(process.pid, 'SIGUSR2');
	});
});

//UNIX-based app termination
process.on('SIGINT', function () {
	gracefulShutdown('app termination', function () {
		process.exit(0);
	});
});

//Heroku app termination
process.on('SIGTERM', function () {
	gracefulShutdown('Heroku app shutdown', function () {
		process.exit(0);
	});
});