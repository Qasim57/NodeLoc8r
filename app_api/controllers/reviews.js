var mongoose = require('mongoose');
var Loc = mongoose.model('Location');

// Selects all reviews for a location, and calls doSetAverageRating (if a location is valid)
var updateAverageRating = function(locationid) {
	Loc
	.findById(locationid)
	.select('rating reviews')
	.exec(
		function (err, location) {
			if(!err) {
				doSetAverageRating(location);
			}
		});
};

// Iterates to determine rating average, and saves that in the parent document
var doSetAverageRating = function(location) {
	var i, reviewCount, ratingAverage, ratingTotal;

	if(location.reviews && location.reviews.length > 0) {
		reviewCount = location.reviews.length;
		ratingTotal = 0;

		for(i = 0; i < reviewCount; i++) {
			ratingTotal = ratingTotal + location.reviews[i].rating;
		}

		ratingAverage = parseInt(ratingTotal / reviewCount, 10);
		location.rating = ratingAverage;

		location.save(function(err){
			if(err){
				console.log(err);
			}else{
				console.log("Average rating updated to ", ratingAverage);
			}
		})
	}
};

// Adds a review to the location passed in from reviewsCreate
var doAddReview = function(req, res, location) {
	if(!location) {
		sendJSONResponse(res, 404, {
			"message": "locationid not found"
		});
	}else{
		location.reviews.push({
			author: req.body.author,
			rating: req.body.rating,
			reviewText: req.body.reviewText
		});

		location.save(function(err, location) {
			var thisReview;

			if (err) {
				console.log(err);
				sendJSONResponse(res, 400, err);
			}else{
				updateAverageRating(location._id);
				thisReview = location.reviews[location.reviews.length - 1];
				sendJSONResponse(res, 201, thisReview);
			}
		});
	}
};

// Create a new review for a location. POST /locations/:locationid/reviews
module.exports.reviewsCreate = function (req, res) {

	var locationid = req.params.locationid;
	if(locationid) {
		Loc.findById(locationid).select('reviews').exec(
			function(err, location){
				if(err) {
					sendJSONResponse(res, 400, err);
				}else{
					doAddReview(req, res, location);
				}
			}
		);
	}else{
		sendJSONResponse(res, 404, {
			"message": "Not found, locationid required"
		});
	}
};

// Returns data for a single review
module.exports.reviewsReadOne = function (req, res) {
	//Verify that we recieved a locationid parameter

	if (req.params && req.params.reviewid && req.params.reviewid) {

		//.exec() executes our query and calls the callback when done. Callback must accept
		//two arguments, error and data
		Loc.findById(req.params.locationid)
		.select('name reviews')
		.exec(
			function(err, location) {

			var response, review;//Response stores our return value. Review points to review contents


			if(!location) {

				sendJSONResponse(res, 404, {
					"message": "No Location with this locationID found"
				});
				return;
			}else if (err) {
				sendJSONResponse(res, 404, err);
				return;
			}

			//Verify that we found reviews
			if (location.reviews && location.reviews.length > 0) {
				review = location.reviews.id(req.params.reviewid);

				if(!review) {
					sendJSONResponse(res, 404, {"message": "No review with this ReviewID found"});
				}else{
					response = {
						location : {
							name: location.name,
							id : req.params.locationid
						},
						review: review
					};

					//Sending review
					sendJSONResponse(res, 200, response);
				}
			}else{
				sendJSONResponse(res, 404, {
					"message": "No reviews found"
				});
			}

		});
	}else{
		sendJSONResponse(res,404, {
			"message": "No locationid in request. Bot"
		});
	}
}

// Updates one review. PUT /locations/:locationid/reviews/:reviewid
module.exports.reviewsUpdateOne = function (req, res) {

	if (!req.params.locationid || !req.params.reviewid) {
		sendJSONResponse(res, 404, {
			"message": "Not found, locationid and reviewid are both required, noLocation: " +  req.params.locationid + " noReview: " + req.params.reviewid
		});
		return;
	}
	Loc.findById(req.params.locationid).select('reviews').exec(function(err, location) {
		var thisReview;

		if (!location) {
			sendJSONResponse(res, 404, {
				"message": "locationid not found"
			});
			return;
		}else if(err){
			sendJSONResponse(res, 400, err);
			return;
		}

		if(location.reviews && location.reviews.length > 0) {

			thisReview = location.reviews.id(req.params.reviewid);

			if(!thisReview) {
				sendJSONResponse(res, 404, {
					"message": "reviewid not found"
				});
			}else{
				thisReview.author = req.body.author;
				thisReview.rating = req.body.rating;
				thisReview.reviewText = req.body.reviewText;

				location.save(function(err, location){
					if(err){
						sendJSONResponse(res, 404, err);
					}else{
						updateAverageRating(location._id);
						sendJSONResponse(res, 200, thisReview);
					}
				});
			}

		}else{
			sendJSONResponse(res, 404, {
				"message": "NO review to update"
			});
		}
	});

}

// Delete one review. DELETE /locations/:locationid/reviews/:reviewid
module.exports.reviewsDeleteOne = function (req, res) {
	
	if (!req.params.locationid || !req.params.reviewid) {
		sendJSONResponse(res, 404, {
			"message": "Not found, locationid and reviewid are both required"
		});
		return;
	}

	Loc.findById(req.params.locationid).select('reviews').exec(function(err, location) {
		if(!location){
			sendJSONResponse(res, 404, {
				"message": "locationid not found"
			});
			return;
		} else if (err) {
			sendJSONResponse(res, 400, err);
			return;
		}

		if(location.reviews && location.reviews.length > 0) {
			
			//Verifying that we have a review with the ID provided in params
			if(!location.reviews.id(req.params.reviewid)) {
				sendJSONResponse(res, 404, {
					"message": "reviewid not found"
				});
			}else{
				
				location.reviews.id(req.params.reviewid).remove();

				location.save(function(err){

					if(err) {
						sendJSONResponse(res, 404, err);
					}else{
						updateAverageRating(location._id);
						sendJSONResponse(res, 204, null);
					}

				});
			}
		}
	});
}


// Shorthand helper method for quickly setting response status/code, content, with the passed-in response object
var sendJSONResponse = function (res, status, content) {
	res.status(status);
	res.json(content);
};