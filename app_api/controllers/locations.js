var mongoose = require('mongoose');
var Loc = mongoose.model('Location');

// Return Locations close to a specified point. GET /locations
module.exports.locationsListByDistance = function (req, res) {

	var lng = parseFloat(req.query.lng);
	var lat = parseFloat(req.query.lat);

	var distance = parseFloat(req.query.maxDistance);
	if(!distance){
		distance = 200000000000000;
	}

	console.log("DISTANCE, ", distance)

	var point = {
		type: "Point",
		coordinates: [lng, lat]
	};
	var geoOptions = {
		spherical : true,
		maxDistance: theEarth.getRadsFromDistance(distance),
		num: 10
	};

	if ((!lng && lng !==0) || (!lat && lat!== 0)) {
		sendJSONResponse(res, 404, {
			"message": "Lng and lat query parameters are required"
		});
		return;
	}

	Loc.geoNear(point, geoOptions, function(err, results, stats){
		if(err){
			sendJSONResponse(res, 404, err.toString());
		}
		var locations = [];
		results.forEach(function(doc){
			locations.push({
				distance: theEarth.getDistanceFromRads(doc.dis),
				name: doc.obj.name,
				address: doc.obj.address,
				rating: doc.obj.rating,
				facilities: doc.obj.facilities,
				_id: doc.obj._id
			});
		});
		sendJSONResponse(res, 200, locations);
	});
}

// Add a new location. POST /locations
module.exports.locationsCreate = function (req, res) {
	//sendJSONResponse(res, 200, {"status" : "success"});
	console.log("REQUEST BODY", req.body);

	//Model.create takes a {data object}, and a callback that gets success/failure info
	Loc.create({
		name: req.body.name,
		address: req.body.address,
		facilities: req.body.facilities.split(","),
		coords: [parseFloat(req.body.lng), parseFloat(req.body.lat)],
		openingTimes: [{
			days: req.body.days1,
			opening: req.body.opening1,
			closing: req.body.closing1,
			closed: req.body.closed1
		}, {
			days: req.body.days2,
			opening: req.body.opening2,
			closing: req.body.closing2,
			closed: req.body.closed2
		}]
	}, function(err, location){
		if (err) {
			sendJSONResponse(res, 400, err.toString());
		}else{
			sendJSONResponse(res, 201, location);
		}
	});
}

// Returns data for a single Location. GET /locations/:locationID
module.exports.locationsReadOne = function (req, res) {

	//Verify that we recieved a locationid parameter
	if (req.params && req.params.locationid) {

		//.exec() executes our query and calls the callback when done. Callback must accept
		//two arguments, error and data
		Loc.findById(req.params.locationid).exec(function(err, location) {
			
			if(!location) {
				sendJSONResponse(res, 404, {
					"message": "No Location with this locationID found"
				});
				return;
			}else if (err) {
				sendJSONResponse(res, 404, err);
				return;
			}

			sendJSONResponse(res, 200, location);
		});
	}else{
		sendJSONResponse(res,404, {
			"message": "No locationid in request"
		});
	}
};

// Update one location. PUT /locations/:locationID
module.exports.locationsUpdateOne = function (req, res) {
	
	if (!req.params.locationid) {
		sendJSONResponse(res, 404, {
			"message": "Not found, locationid is required"
		});
		return;
	}

	Loc.findById(req.params.locationid)
	.select('-reviews -rating')
	.exec(
		function(err, location){
			if(!location) {
				
				sendJSONResponse(res, 404, {
					"message": "locationid not found"
				});
				return;
			}else if(err) {
				sendJSONResponse(res, 400, err);
				return;
			}

			location.name = req.body.name;
			location.address = req.body.address;
			location.facilities = req.body.facilities.split(",");
			location.coords = [parseFloat(req.body.lng), parseFloat(req.body.lat)];

			location.openingTimes = [{
				days: req.body.days1,
				opening: req.body.opening1,
				closing: req.body.closing1,
				closed: req.body.closed1
			}, {
				days: req.body.days2,
				opening: req.body.opening2,
				closing: req.body.closing2,
				closed: req.body.closed2
			}];

			location.save(function(err, location) {
				if (err) {
					sendJSONResponse(res, 404, err);
				} else {
					sendJSONResponse(res, 200, location);
				}
			});
		})
}

// Delete one location. DELETE /locations/:locationID
module.exports.locationsDeleteOne = function (req, res) {
	
	var locationid = req.params.locationid;

	if(locationid) {

		Loc.findByIdAndRemove(locationid).exec(function(err, location){
			if (err) {
				sendJSONResponse(res, 404, err.toString());
				return;
			}

			sendJSONResponse(res, 204, null);
		});

	}else{
		sendJSONResponse(res, 404, {
			"message": "No locationid"
		});
	}
}

// Helper method for sending JSON. We pass this our request object, and this renders a HTTP status and a json response using that req obj
var sendJSONResponse = function (res, status, content) {
	res.status(status);
	res.json(content);
};

// Helper methods for converting radian values, to get the distance from a radian angle (and getting a radian angle from distance)
var theEarth = (function(){
	var earthRadius = 6371;//km, miles is 3959

	var getDistanceFromRads = function(rads) {
		return parseFloat(rads * earthRadius);
	}

	var getRadsFromDistance = function(distance) {
		return parseFloat(distance / earthRadius);
	}

	return {
		getDistanceFromRads : getDistanceFromRads,
		getRadsFromDistance : getRadsFromDistance
	}
})();